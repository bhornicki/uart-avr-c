/*
 * Based on Miroslaw Kardas (Mirekk36 library)
 * Modified by HorochovPL
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <math.h>
#include "uart.h"


//Transtates "universal" names to probably working specific ones.
#ifndef UDR
	#define RXCIE	RXCIE0
	#define TXCIE	TXCIE0
	#define RXEN	RXEN0
	#define TXEN	TXEN0
	#define UBRRH	UBRR0H
	#define UBRRL	UBRR0L
	#define UCSRA	UCSR0A
	#define UCSRB	UCSR0B
	#define UCSRC	UCSR0C
	#define UCSZ0	UCSZ00
	#define UCSZ1	UCSZ01
	#define UPM0	UPM00
	#define UPM1	UPM01
	#define USBS	USBS0
	#define UDR		UDR0
	#define UDRIE	UDRIE0
	#define U2X		U2X0

	#define USART_RXC_vect USART_RX_vect
	#define USART_TXC_vect USART_TX_vect
#endif

//Read
#if UART_RX_BUF_SIZE>0
	static volatile char UART_RxBuf[UART_RX_BUF_SIZE];
	static volatile uint8_t UART_RxHead;	//exactly at last in
	static volatile uint8_t UART_RxTail;	//just behind first in
#endif

//Transmit
#if UART_TX_BUF_SIZE>0
	static volatile char UART_TxBuf[UART_TX_BUF_SIZE];
	static volatile uint8_t UART_TxHead;
	static volatile uint8_t UART_TxTail;
#endif



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 																		 *
 * 					I N I T												 *
 * 																		 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 * Remember about SEI later!
 * Check |F_CPU <-> baud| error dependencies table!
 * Will automatically set U2X if it would decrease error
 * Default 9600
 */
void uart_init (uint16_t baud)
{
	uint8_t u2x=1;
	if(abs((int)(1000*uart_getBaudError(baud,0)))>
	   abs((int)(1000*uart_getBaudError(baud,1))))
		u2x=2;

	uart_initAdv((F_CPU+baud*4*(u2x)) / ((unsigned long)baud*8*(u2x))-1,2-u2x);
}

/*
 * Remember about SEI later!
 * Check |F_CPU <-> baud| error dependencies table!
 * 	insert ubrr, u2x value from mentioned table
 * 	Beware the double-init!
 */
void uart_initAdv( uint16_t ubrr, uint8_t u2x)
{
	#ifdef URSEL
		//Trap specific to some MCUs: two registers under the same adress, changed by one bit:
		//URSEL = 0 -> UBBRH
		//URSEL = 1 -> UCSRC
		//Clear UCSRC, with URSEL
		UCSRC = 0;
	#endif
	/* Speeds */
	UBRRH = (uint8_t)(ubrr>>8);
	UBRRL = (uint8_t)ubrr;
	if(u2x)
		UCSRA|=(1<<U2X);
	else
		UCSRA&=~(1<<U2X);

	#ifdef URSEL
		UCSRC = (1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1);
	#else
		UCSRC = (1<<UCSZ0)|(1<<UCSZ1);
	#endif

	UCSRB = (1<<RXEN)|(1<<TXEN)|(1<<RXCIE);
	#if _UART_USE_RS485>0
		UART_DE_DIR |= UART_DE_BIT;
		UART_DE_RECEIVING;
		//Attach TX interrupt
		UCSRB |= (1<<TXCIE);
	#endif
}
/*
 * Remember about SEI later!
 * Check |F_CPU <-> baud| error dependencies table!
 * insert ubrr, u2x value from mentioned table
 * Parity, stopbits and charsize are declared in enums, starting with "uart" word.
 */
void uart_initAdv2( uint16_t ubrr, uint8_t u2x, uint8_t parity, uint8_t stopBits,uint8_t charSize)
{
	#ifdef URSEL
		//Trap specific to some MCUs: two registers under the same adress, changed by one bit.
		//URSEL = 0 -> UBBRH
		//URSEL = 1 -> UCSRC
		UCSRC = 0;
	#endif
	/* Speeds */
	UBRRH = (uint8_t)(ubrr>>8);
	UBRRL = (uint8_t)ubrr;
	if(u2x)
		UCSRA|=(1<<U2X);
	else
		UCSRA&=~(1<<U2X);

	#ifdef URSEL
		UCSRC = (1<<URSEL)|((charSize&3)<<UCSZ0)|((stopBits&1)<<USBS)|((parity&3)<<UPM0);
	#else
		UCSRC = ((charSize&3)<<UCSZ0)|((stopBits&1)<<USBS)|((parity&3)<<UPM0);
	#endif

	UCSRB = (1<<RXEN)|(1<<TXEN)|(1<<RXCIE);
	#if _UART_USE_RS485>0
		UART_DE_DIR |= UART_DE_BIT;
		UART_DE_RECEIVING;
		//Attach TX interrupt
		UCSRB |= (1<<TXCIE);
	#endif
}


/*
 * Calculates baudrate error. The lower the better.
 */
float uart_getBaudError(uint16_t baud, uint8_t u2x)
{
	u2x=(u2x?1:2);
	unsigned long int ubrr=(F_CPU+(4*baud*u2x))/((unsigned long int)9600*8*u2x)-1;

	return  ((float)F_CPU/((float)8*u2x*(ubrr+1)*baud)-1);
}

//Interrupt Tx Complete, when UDR0 emptied.
#if _UART_USE_RS485>0
ISR( USART_TXC_vect )
{
	UART_DE_PORT &= ~UART_DE_BIT;	// lock RS485 transmitter
}
#endif



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 																		 *
 * 					T R A N S M I T										 *
 * 																		 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#if UART_TX_BUF_SIZE>0

void uart_char( char data )
{
	UART_TX_TYPE tmp_head;
    tmp_head  = (UART_TxHead + 1) & UART_TX_BUF_MASK;

    //wait if buffer is filled
    while ( tmp_head == UART_TxTail );

    UART_TxBuf[tmp_head] = data;
    UART_TxHead = tmp_head;

    //init buffer empty interrupt. ISR will do the rest.
    UCSRB |= (1<<UDRIE);
}

/*
 * Converts value to text.
 */
void uart_number(int value, int base)
{
	char string[17];			//buffer
	itoa(value, string, base);	//value to ASCII
	uart_string(string);		//send
}

void uart_numberDec(int value)
{
	char string[6];				//buffer
	itoa(value, string, 10);	//value to ASCII
	uart_string(string);		//send
}

void uart_string(char *s)
{
  register char c;
  //add to buffer until \0 occurs
  while ((c = *s++))
	  uart_char(c);
}

/*
 * Will send string encoded in flash.
 * Use pgmspace.h
 * You can use uart_stringFlash(PSTR("YourString");
 */
void uart_stringFlash(const char *str){
	 register char c;
	  while ((c = (char) pgm_read_byte(str++)))
		  uart_char(c);
}
void uart_endline(void)
{
	uart_stringFlash(PSTR("\r\n"));
}

void uart_fillLine(char ascii, uint8_t cnt)
{
	for(uint8_t i=0; i<cnt; i++)
		uart_char(ascii);
}

UART_TX_TYPE uart_howManyToWrite(void)
{
	return UART_TxHead-UART_TxTail+((UCSRB>>UDRIE)&1);
}

//send interrupt
ISR( USART_UDRE_vect)
{
    //anything to send?
    if ( UART_TxHead != UART_TxTail )
    {
    	UART_TxTail = (UART_TxTail + 1) & UART_TX_BUF_MASK;
    	//load from transmit buffer
    	UDR = UART_TxBuf[UART_TxTail];
    }
    else
    {
    	//disable interrupt
    	UCSRB &= ~(1<<UDRIE);
    }
}

#endif //T R A N S M I T

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 																		 *
 * 					R E C E I V E										 *
 * 																		 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#if UART_RX_BUF_SIZE>0

char uart_getChar(void)
{
	if ( UART_RxHead == UART_RxTail )
    	return 0;

    UART_RxTail = (UART_RxTail + 1) & UART_RX_BUF_MASK;
    return UART_RxBuf[UART_RxTail];
}

UART_RX_TYPE uart_findChar(char chr)
{
	if (UART_RxTail == UART_RxHead)
		return 0;

	UART_RX_TYPE pos = 0;
	for (UART_RX_TYPE i = 1; i <= UART_RxHead-UART_RxTail; ++i)
	{
		//tail is just behind 1st data to read. Temp version points directly.
		UART_RX_TYPE tempTail = (UART_RxTail + i) & UART_RX_BUF_MASK;

		if (UART_RxBuf[tempTail] == chr)
		{
			pos = i;
			break;
		}
	}
	return pos;
}
/*
 * -1 = no string
 * 0 = just \r\n
 */
int16_t uart_getStrLen(void)
{
	int16_t n=(int16_t)uart_findChar('\n')-1;
	if(uart_findChar('\r')==n)
		n--;
	return n;

}


/*
 * Returns lenghth of string including \0.
 * Only full line will be returned!
 */
UART_RX_TYPE uart_getLine(char* msg)
{
	UART_RX_TYPE len=uart_findChar('\n'),
			finalLen=0;
	if(len)
	{
		while(len--)
		{
			char chr=uart_getChar();
			if(chr!='\r'&&chr!='\n'&&chr!=0)
			{
				if(msg)
					*(msg++)=chr;
				finalLen++;
			}
			else
			{
				//in case \r\n ending, skip \n
				if ( UART_RxHead == UART_RxTail )
					break;
				UART_RX_TYPE tempTail = (UART_RxTail + 1) & UART_RX_BUF_MASK;
				if(UART_RxBuf[tempTail]=='\n')
					UART_RxTail=tempTail;
					break;
			}
		}
		//end string with \0
		if(msg)
			*(msg)='\0';
		finalLen++;
	}
	return finalLen;
}

//receive interrupt
ISR(USART_RXC_vect) {
	UART_RX_TYPE tmp_head;
    char data;

    data = UDR;

	#if  _UART_RX_NEWLINE_TYPE>0
		if(data=='\r')
			return;
	#endif
	#if _UART_RX_SINGLE_NEWLINE>0
		//Lets assume that \r\r\r... won't occur
		if((data=='\r'||data=='\n')&&(UART_RxBuf[UART_RxHead]=='\n'))
			return;
	#endif

    //new head
    tmp_head = ( UART_RxHead + 1) & UART_RX_BUF_MASK;

    //Do we have space for new data?
    if ( tmp_head == UART_RxTail )
    {
    	//Process overfilling here
    	//#TODO: Maybe callback
    	return;
    }

    //save new head and write data into RxBuf
	UART_RxHead = tmp_head;
	UART_RxBuf[tmp_head] = data;

}
#endif	//R E C E I V E
