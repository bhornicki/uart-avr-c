# avrUart
This code implements UART. 

You can use standard #include "avrUart/uart.h" for basic functionality, or #include "avrUart/uartTerminal.h" for extended functionality, like changing text color, backgroud - not all programs will be compatible with those commands!

# Functions
## void UART_Init(uint16_t baud)
Start UART with 0 parity bits, 1 stop and 8bit length. Bauds 9600 or 19200 are defaults in most cases. Check "Examples of Baud Rate Setting" chapter in your MCU datasheet.
## void UART_InitAdv(uint16_t ubrr, uint8_t u2x)
Start UART with 0 parity bits, 1 stop and 8bit length. Manual baud rate, check "Examples of Baud Rate Setting" chapter in your MCU datasheet.
## int uart_getBaudError(uint16_t baud, uint8_t u2x)
Returns calculated transmission error in percents.


