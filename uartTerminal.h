/*
 * Based on Miroslaw Kardas (Mirekk36 library)
 * Modified by HorochovPL
 * To use polish chars in PUTTY -> set WINDOW / Translation / Win1250
 */
#ifndef UART_TERMINAL_H_
#define UART_TERMINAL_H_
#include <stdint.h>
#include "uart.h"

/*
 * THOSE ARE FIXED VALUES!
 */
enum uartCharAttrib
{
	uartChrAtrReset=0,
	uartChrAtrBold,
	uartChrAtrDim,
	uartChrAtrUnderline,
	uartChrAtrBlink,
	uartChrAtrReverse=7,
	uartChrAtrHidden
};
enum uartColors
{
	uartColBlack=0,
	uartColRed,
	uartColGreen,
	uartColYellow,
	uartColBlue,
	uartColMagenta,
	uartColCyan,
	uartColWhite
};

void uart_cls(uint8_t isCursorVisible);
void uart_setPosition(uint8_t x, uint8_t y);

void uart_setCursorVis(uint8_t isVisible);
void uart_setAttr(uint8_t attr, uint8_t color, uint8_t bcgr);
void uart_setFontColor(uint8_t color);
void uart_setBackgroundColor( uint8_t bcgr);



#endif /* UART_TERMINAL_H_ */
