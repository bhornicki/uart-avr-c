#ifndef CONFIG_H
#define CONFIG_H

/*
 * Use powers of 2
 * If 0 used, functionality will be turned off
 */
#define UART_RX_BUF_SIZE 64		//Read
#define UART_TX_BUF_SIZE 32		//Transmit

/*
 * Blindly reimplemented, probably doesn't work.
 */
#define _UART_USE_RS485 0

/*
 * Newline type 0 = do not change string
 * Newline type 1 = transform incoming \r\n to \n
 *
 * Single newline 1 = transform (\r)\n(\r)\n... to single (\r)\n
 */
#define _UART_RX_NEWLINE_TYPE 0
#define _UART_RX_SINGLE_NEWLINE 0

#if _UART_USE_RS485>0
	#define UART_DE_PORT PORTD
	#define UART_DE_DIR DDRD
	#define UART_DE_BIT (1<<PD2)
#endif

#endif // CONFIG_H
