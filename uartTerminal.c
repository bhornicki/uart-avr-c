/*
 * Based on Miroslaw Kardas (Mirekk36 library)
 * Modified by HorochovPL
 * To use polish chars in PUTTY -> set WINDOW / Translation / Win1250
 */
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "uartTerminal.h"


const char UCLS[] PROGMEM = {"\x1b""[2J"};

const char UHOME[] PROGMEM = {"\x1b""[;H"};

const char UCUR_HIDE[] PROGMEM = {"\x1b""[?25l"};
const char UCUR_SHOW[] PROGMEM = {"\x1b""[?25h"};

const char U_ATTR_OFF[] PROGMEM = {"\x1b""[m"};


void uart_cls(uint8_t isCursorVisible)
{
	uart_stringFlash(U_ATTR_OFF);
	uart_setCursorVis(isCursorVisible);
	uart_stringFlash(UCLS);

	uart_stringFlash(UHOME);
}

void uart_setPosition(uint8_t x, uint8_t y)
{
	// <ESC>[y;xH
	uart_char(0x1b);
	uart_char('[');
	uart_numberDec(y);
	uart_char(';');
	uart_numberDec(x);
	uart_char('H');
}

void uart_setCursorVis(uint8_t isVisible)
{
	if(isVisible)
		uart_stringFlash(UCUR_SHOW);
	 else
		uart_stringFlash(UCUR_HIDE);
}


void uart_setAttr(uint8_t attr, uint8_t color, uint8_t bcgr)
{
	// <ESC>[0;32;44m
	uart_char(0x1b);
	uart_char('[');
	uart_char(attr+'0');
	uart_char(';');
	uart_char('3');
	uart_char(color+'0');
	uart_char(';');
	uart_char('4');
	uart_char(bcgr+'0');
	uart_char('m');
}


void uart_setFontColor(uint8_t color)
{
	// <ESC>[34m
	uart_char(0x1b);
	uart_char('[');
	uart_char('3');
	uart_char(color+'0');
	uart_char('m');
}

void uart_setBackgroundColor(uint8_t bcgr)
{
	// <ESC>[44m
	uart_char(0x1b);
	uart_char('[');
	uart_char('4');
	uart_char(bcgr+'0');
	uart_char('m');
}
