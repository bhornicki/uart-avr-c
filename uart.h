/*
 * Based on Miroslaw Kardas (Mirekk36 library)
 * Modified by HorochovPL
 */
#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include "config.h"

#if UART_TX_BUF_SIZE>255
	#define UART_TX_TYPE uint16_t
#else
	#define UART_TX_TYPE uint8_t
#endif
#if UART_RX_BUF_SIZE>255
	#define UART_RX_TYPE uint16_t
#else
	#define UART_RX_TYPE uint8_t
#endif

#if UART_TX_BUF_SIZE>0
	#define UART_TX_BUF_MASK ( UART_TX_BUF_SIZE - 1)
	void uart_char(char data);
	void uart_number(int value, int base);
	void uart_numberDec(int value);
	void uart_string(char *s);
	void uart_stringFlash(const char *str);
	void uart_endline(void);
	void uart_fillLine(char ascii, uint8_t cnt);
	UART_TX_TYPE uart_howManyToWrite(void);
#endif


#if UART_RX_BUF_SIZE>0
	#define UART_RX_BUF_MASK ( UART_RX_BUF_SIZE - 1)
	char uart_getChar(void);
	UART_RX_TYPE uart_findChar(char chr);
	int16_t uart_getStrLen(void);
	UART_RX_TYPE uart_getLine(char* msg);
#endif

void uart_init(uint16_t baud);
void uart_initAdv(uint16_t ubrr, uint8_t u2x);
void uart_initAdv2( uint16_t ubrr, uint8_t u2x, uint8_t parity, uint8_t stopBits,uint8_t charSize);
float uart_getBaudError(uint16_t baud, uint8_t u2x);

#if _UART_USE_RS485>0
	#define UART_DE_RECEIVING  UART_DE_PORT |= UART_DE_BIT
	#define UART_DE_TRANSMITTING  UART_DE_PORT &= ~UART_DE_BIT
#endif

enum uartParity
{
	uartParityNone=0,
	uartParityEven=2,
	uartParityOdd=3
};
enum uartStopBits
{
	uartStopBitOne,
	uartStopBitsTwo
};

enum uartCharacterSize
{
	uartCharSize5bit,
	uartCharSize6bit,
	uartCharSize7bit,
	uartCharSize8bit,
//	uartCharSize9bit=7
};

#endif /* UART_H_ */
